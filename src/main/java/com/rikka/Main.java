package com.rikka;

import javax.annotation.Resource;
import java.sql.SQLOutput;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        ergodicList();
//        sort();
//        add();
//        findMax();
    }


    //1.集合遍历
    private static void ergodicList() {
        List<String> list = Arrays.asList("阿伟寄了", "你挑的吗", "偶像");
        //1.1 常规遍历①
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        //1.2 常规遍历②
        for (String var : list) {
            System.out.println(var);
        }
        //1.3 forEach遍历
        list.forEach( var -> {
            System.out.println(var);
        });
        //1.4 更进阶写法
        list.forEach(System.out::println);
        //1.5 没怎么见过的写法
        System.out.println(Stream.iterate(0, i -> i++).limit(list.size()).map(var ->{
            return list.get(var);
        }).collect(Collectors.toList()));
    }

    //2.排序
    private static void sort(){
        List<String> list = Arrays.asList("阿伟寄了", "你挑的吗", "偶像");
        //2.1 普通排序
        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        //2.2 lambda写法
        Collections.sort(list, ((o1, o2) -> o1.compareTo(o2)));
        //2.3 更进阶写法
        Collections.sort(list, (String::compareTo));
        System.out.println(list);
        //2.4 stream写法
        list = list.stream().sorted()
        .collect(Collectors.toList());

    }

    //3.集合加法操作
    public static void add() {
        List<Integer> list = Arrays.asList(1, 2, 3);
        //3.1 普通写法
        int sum = 0;
        for (Integer i : list) {
            sum += i;
        }
        System.out.println(sum);


        //3.2 steam写法
        System.out.println(list.stream().reduce(0, Integer::sum));
    }

    //4.找最大
    public static void findMax(){
        List<Integer> list = Arrays.asList(1, 2, 3,1);
        list.stream().max((Integer::compareTo)).ifPresent(System.out::println);
    }

    //5.分组操作
    public static void group(){
        List<String> list = Arrays.asList("阿伟寄了", "你挑的吗", "偶像","是我");
        //5.1 常规分组写法
        Map<Integer, List<String>> groups1 = new HashMap<>();
        for (String var : list) {
            int length = var.length();
            if (!groups1.containsKey(length)) {
                groups1.put(length, new ArrayList<>());
            }
            groups1.get(length).add(var);
        }
        System.out.println(groups1);
        //5.2 stream写法
        Map<Integer, List<String>> groups2 = list.stream().collect(Collectors.groupingBy(String::length));
    }

    //6.线程写法
    public static void thread() {
        //6.1 常规写法
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("a");
            }
        }).start();

        //6.2 lambda写法
        new Thread( () -> System.out.println("a")).start();
    }

    //7.条件判断操作
    private static void judgment() {
        String a = "abc";
        //1.普通写法
        if (a != null) {
            System.out.println(a.toUpperCase());
        }

        //2. 特殊写法
        Optional.ofNullable(a).map(String::toUpperCase).ifPresent(System.out::println);
    }
}